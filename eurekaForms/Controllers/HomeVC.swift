//
//  HomeVC.swift
//  eurekaForms
//
//  Created by Apple on 6/4/20.
//  Copyright © 2020 Appshap.co. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }
}
