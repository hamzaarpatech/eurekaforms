//
//  LoginVC.swift
//  eurekaForms
//
//  Created by Apple on 6/3/20.
//  Copyright © 2020 Appshap.co. All rights reserved.
//

import UIKit
import Eureka

class LoginVC: FormViewController , UITextFieldDelegate {
    
    let email_row_tag = "Email"
    let pass_row_tag = "Password"
    let login_bt_row_tag = "Login"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.tableView.separatorStyle = .none
        self.rowKeyboardSpacing = 60
        self.tableView.keyboardDismissMode = .interactive
       
        form
            +++ Section { section in
                var header = HeaderFooterView<LoginHeader>(.nibFile(name:"LoginHeader",bundle:nil))
                header.height = {300}
                header.onSetupView = { view, _ in
                    view.loginTitle.text = "Login to your account"
                    view.backgroundColor = UIColor.clear
                }
                section.header = header
        }
        
        <<< CustomTextFieldRow(email_row_tag) { row in
            row.cell.mytextfield.delegate = self
            row.cell.mytextfield.returnKeyType = .next
            row.cell.selectionStyle = .none
            row.cell.mytextfield.placeholder = "Email"
            row.cell.backgroundColor = UIColor.clear
            row.cell.mytextfield.keyboardType = .emailAddress
            row.cell.mytextfield.setTextfieldImage(#imageLiteral(resourceName: "mail"))
            row.add(rule: RuleRequired(msg: "Email field is required.", id: nil))
            row.add(rule: RuleEmail(msg: "Email field must be a valid email.", id: nil))
            row.onChange { (row) in
            row.cell.mytextfield.text = row.value
                  }
            row.onCellSelection({ (cell, row) in
                cell.mytextfield.becomeFirstResponder()
                })
            }
              
         <<< CustomTextFieldRow(pass_row_tag) { row in
            row.cell.mytextfield.delegate = self
            row.cell.mytextfield.returnKeyType = .done
            row.cell.selectionStyle = .none
            row.cell.mytextfield.placeholder = "Password"
            row.cell.backgroundColor = UIColor.clear
            row.cell.mytextfield.keyboardType = .default
            row.cell.mytextfield.isSecureTextEntry = true
            row.cell.mytextfield.setTextfieldImage(#imageLiteral(resourceName: "password"))
            row.add(rule: RuleRequired(msg: "Password field is required.", id: nil))
            row.onCellSelection({ (cell, row) in
            cell.mytextfield.becomeFirstResponder()
                })
            row.onChange { (row) in
                row.cell.mytextfield.text = row.value
                }
            }
            
         <<< CustomButtonRow(login_bt_row_tag) { row in
            row.cell.selectionStyle = .none
            row.cell.backgroundColor = .clear
            row.cell.onButtonPress {
                self.view.endEditing(true)
                for row in row.section?.form?.validate() ?? [] {
                    let alert = UIAlertController(title: "Note", message: row.msg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true)
                    return
                }
                self.performSegue(withIdentifier: "gotoHomeVC", sender: Any?.self)
            }
        }
    }
}
