//
//  CustomTextFieldCell.swift
//  eurekaForms
//
//  Created by Apple on 6/3/20.
//  Copyright © 2020 Appshap.co. All rights reserved.
//

import UIKit
import Eureka

class CustomTextFieldCell: Cell<String>, CellType {

    @IBOutlet weak var mytextfield: UITextField!
    
    override public func setup() {
        mytextfield.autocorrectionType = .no
        mytextfield.layer.cornerRadius = 2.0
        mytextfield.addTarget(self, action: #selector(textChanged), for: .allEditingEvents)
    }
    
    @objc func textChanged() {
        row.value = mytextfield.text
    }
}

 final class CustomTextFieldRow: Row<CustomTextFieldCell>, RowType{
    required public init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<CustomTextFieldCell>(nibName: "CustomeTextFieldCell")
    }
}
