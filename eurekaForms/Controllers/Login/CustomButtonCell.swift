//
//  CustomButtonCell.swift
//  eurekaForms
//
//  Created by Apple on 6/3/20.
//  Copyright © 2020 Appshap.co. All rights reserved.
//

import UIKit
import Eureka

class CustomButtonCell: Cell<String>, CellType {
    @IBOutlet weak var loginBt: UIButton!
    var buttonPressCallBack: (()->Void)?
    
    public func onButtonPress(callBack: @escaping (()->Void)) {
        self.buttonPressCallBack = callBack
    }
    
    public override func setup() {
        loginBt.layer.cornerRadius = 2.0
        loginBt.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
    }

    @objc func buttonPressed(sender: UIButton) {
        self.buttonPressCallBack?()
    }
}

  final class CustomButtonRow: Row<CustomButtonCell>, RowType {
    required public init(tag: String?) {
    super.init(tag: tag)
    cellProvider = CellProvider<CustomButtonCell>(nibName: "CustomButtonCell")
   }
}
