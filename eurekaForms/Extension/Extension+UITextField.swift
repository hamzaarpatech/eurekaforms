//
//  Extension+UITextField.swift
//  eurekaForms
//
//  Created by Apple on 6/3/20.
//  Copyright © 2020 Appshap.co. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    func setTextfieldImage(_ image: UIImage) {
       let iconView = UIImageView(frame:
                      CGRect(x: 10, y: 5, width: 20, height: 20))
       iconView.image = image
       let iconContainerView: UIView = UIView(frame:
                      CGRect(x: 20, y: 0, width: 40, height: 30))
       iconContainerView.addSubview(iconView)
       leftView = iconContainerView
       leftViewMode = .always
    }
}
